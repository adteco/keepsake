<?php
/**
 * Paradox Labs, Inc.
 * http://www.paradoxlabs.com
 * 717-431-3330
 *
 * Need help? Open a ticket in our support system:
 *  http://support.paradoxlabs.com
 *
 * @author      Chad Bender <support@paradoxlabs.com>
 * @license     http://store.paradoxlabs.com/license.html
 */

namespace ParadoxLabs\FirstData\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\PaymentException;

/**
 * First Data API Gateway - custom built for perfection.
 */
class Gateway extends \ParadoxLabs\TokenBase\Model\AbstractGateway
{
    /**
     * @var string
     */
    protected $code = 'paradoxlabs_firstdata';

    /**
     * @var string
     */
    protected $endpointLive = 'https://api.payeezy.com/v1';

    /**
     * @var string
     */
    protected $endpointTest = 'https://api-cert.payeezy.com/v1';


    /**
     * @var string
     */
    protected $transarmorToken;

    /**
     * $fields defines validation for each API parameter or input.
     *
     * key => [
     *    'maxLength' => int,
     *    'noSymbols' => true|false,
     *    'charMask'  => (allowed characters in regex form),
     *    'enum'      => [ values ]
     * ]
     *
     * @var array
     */
    protected $fields = [
        //Tokenize Credit Card Fields
        'apikey'                            => [],
        'ta_token'                          => [],
        'auth'                              => ['enum' => ['true', 'false']],
        'credit_card_type'                  => [
            'enum' => [
                'visa',
                'mastercard',
                'american express',
                'diners',
                'discover',
                'jcb',
            ],
        ],
        'cardholder_name'                   => ['maxLength' => 100, 'noSymbols' => true],
        'card_number'                       => ['maxLength' => 16, 'charMask' => 'X\d'],
        'currency'                          => ['maxLength' => 3],
        'exp_date'                          => ['maxLength' => 4, 'charMask' => '\d'],
        'cvv'                               => ['maxLength' => 4, 'charMask' => '\d'],
        'billing_address_city'              => ['maxLength' => 40, 'noSymbols' => true],
        'billing_address_country'           => ['maxLength' => 2, 'noSymbols' => true],
        'billing_address_email'             => ['maxLength' => 255],
        'billing_address_street'            => ['maxLength' => 60, 'noSymbols' => true],
        'billing_address_phone_number'      => ['maxLength' => 25, 'charMask' => '\d\(\)\-\.'],
        'billing_address_state_province'    => ['maxLength' => 2, 'noSymbols' => true],
        'billing_address_zip_postal_code'   => ['maxLength' => 20, 'noSymbols' => true],
        //Token Based Payment Fields
        'token'                             => [],
        'method'                            => ['enum' => ['token']],
        'transaction_type'                  => [
            'enum' => [
                'purchase',
                'authorize',
                'capture',
                'refund',
                'void',
            ],
        ],
        'amount'                            => [],
        'currency_code'                     => ['maxLength' => 3],
        'token_data_value'                  => [],
        'transId'                           => [],
        'transaction_tag'                   => [],
        'merchant_ref'                      => [],
    ];

    /**
     * @var \Magento\Framework\Module\Dir
     */
    protected $moduleDir;

    /**
     * @var array|string
     */
    protected $lastResponse;

    /**
     * Gateway constructor.
     *
     * @param \ParadoxLabs\FirstData\Helper\Data $helper
     * @param \ParadoxLabs\TokenBase\Model\Gateway\Xml $xml
     * @param \ParadoxLabs\TokenBase\Model\Gateway\ResponseFactory $responseFactory
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Magento\Framework\Module\Dir $moduleDir
     * @param array $data
     */
    public function __construct(
        \ParadoxLabs\FirstData\Helper\Data $helper,
        \ParadoxLabs\TokenBase\Model\Gateway\Xml $xml,
        \ParadoxLabs\TokenBase\Model\Gateway\ResponseFactory $responseFactory,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Framework\Module\Dir $moduleDir,
        array $data = []
    ) {
        $this->moduleDir = $moduleDir;

        parent::__construct(
            $helper,
            $xml,
            $responseFactory,
            $httpClientFactory,
            $data
        );
    }

    /**
     * Initialize the gateway. Input is taken as an array for greater flexibility.
     *
     * @param array $parameters
     * @return $this
     */
    public function init(array $parameters)
    {
        $this->transarmorToken = $parameters['ta_token'];

        parent::init($parameters);

        return $this;
    }

    /**
     * Set the API credentials so they go through validation.
     *
     * @return $this
     * @throws PaymentException
     */
    public function clearParameters()
    {
        parent::clearParameters();

        if (isset($this->defaults['login'], $this->defaults['password'])) {
            $this->setParameter('apikey', $this->defaults['login']);
            $this->setParameter('token', $this->defaults['password']);
        }

        return $this;
    }

    /**
     * Send the given request to First Data and process the results.
     *
     * @param array $params
     * @param string $path
     * @param bool $isApiTest
     * @return array
     * @throws LocalizedException
     */
    protected function runTransaction($params, $path, $isApiTest = false)
    {
        $auth = [
            'apikey'    => $this->getParameter('apikey'),
            'token'     => $this->getParameter('token'),
        ];

        /**
         * Check to see if transId is set to add it to endpoint
         * transId is needed for capture/invoice only, refund, and void
         */
        if ($this->hasParameter('transId')) {
            $path .= '/' . substr($this->getParameter('transId'), 0, strcspn($this->getParameter('transId'), '-'));
        }

        $httpClient = $this->getHttpClient($path);

        $paramData = json_encode($params);

        $header = $this->createHeader($auth, $paramData);

        $httpClient->setHeaders($header);

        $this->lastRequest = $paramData;

        try {
            $httpClient->setRawData($paramData, 'application/json');
            $response = $httpClient->request(\Zend_Http_Client::POST);
            $responseBody = $response->getBody();

            $this->lastResponse = $responseBody;

            $this->handleResponse($httpClient, json_decode($paramData, true), $isApiTest);
        } catch (\Zend_Http_Exception $e) {
            $this->helper->log(
                $this->code,
                sprintf(
                    "CURL Connection error: %s. %s (%s)\nREQUEST: %s",
                    $e->getMessage(),
                    $httpClient->getAdapter()->getError(),
                    $httpClient->getAdapter()->getErrno(),
                    $this->sanitizeLog(json_decode($paramData, true))
                )
            );

            throw new LocalizedException(
                __(sprintf(
                    'First Data Gateway Connection error: %s. %s (%s)',
                    $e->getMessage(),
                    $httpClient->getAdapter()->getError(),
                    $httpClient->getAdapter()->getErrno()
                ))
            );
        }

        return $this->lastResponse;
    }

    /**
     * Gets the HTTP Client to be used for transactions
     *
     * @param string $path
     * @return \Magento\Framework\HTTP\ZendClient
     */
    protected function getHttpClient($path)
    {
        /** @var \Magento\Framework\HTTP\ZendClient $httpClient */
        $httpClient = $this->httpClientFactory->create();

        $clientConfig = [
            'adapter'     => \Zend_Http_Client_Adapter_Curl::class,
            'timeout'     => 15,
            'curloptions' => [
                CURLOPT_CAINFO         => $this->moduleDir->getDir('ParadoxLabs_FirstData') . '/firstdata-cert.pem',
                CURLOPT_SSL_VERIFYPEER => false,
            ],
            'verifypeer' => false,
            'verifyhost' => 0,
        ];

        if ($this->verifySsl === true) {
            $clientConfig['curloptions'][CURLOPT_SSL_VERIFYPEER] = true;
            $clientConfig['curloptions'][CURLOPT_SSL_VERIFYHOST] = 2;
            $clientConfig['verifypeer'] = true;
            $clientConfig['verifyhost'] = 2;
        }

        $httpClient->setUri($this->endpoint . $path);
        $httpClient->setConfig($clientConfig);

        return $httpClient;
    }

    /**
     * Mask certain values in the response for secure logging purposes.
     *
     * @param $array
     * @return mixed
     */
    protected function sanitizeLog($array)
    {
        $maskAll  = ['cvv', 'ta_token'];
        $maskFour = ['card_number'];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[ $key ] = $this->sanitizeLog($value);
            } elseif (in_array($key, $maskAll)) {
                $array[ $key ] = 'XXX';
            } elseif (in_array($key, $maskFour)) {
                $array[ $key ] = 'XXXX' . substr($value, -4);
            }
        }

        return json_encode($array);
    }

    /**
     * Turn transaction results and directResponse into a usable object.
     *
     * @param array $transactionResult
     * @return \ParadoxLabs\TokenBase\Model\Gateway\Response
     * @throws LocalizedException
     * @throws PaymentException
     */
    protected function interpretTransaction($transactionResult)
    {
        /**
         * Turn response into a consistent data object, as best we can
         */
        $data = $this->getDataFromResponse($transactionResult);

        /** @var \ParadoxLabs\TokenBase\Model\Gateway\Response $response */
        $response = $this->responseFactory->create();
        $response->setData($data);

        // Unclear on 17059, but the other fraud codes are detailed here: https://goo.gl/9HkMvb
        if (in_array($response->getResponseCode(), [17059, 200, 503, 596], false)) {
            $response->setIsFraud(true);
        }

        return $response;
    }

    /**
     * Tokenize credit card to get token
     *
     * @param bool $isApiTest
     * @return string First Data token
     * @throws PaymentException
     * @throws LocalizedException
     */
    public function tokenizeCreditCard($isApiTest = false)
    {
        $params = [];
        $params = $this->tokenizeCreditCardAddPaymentInfo($params);
        $params['ta_token'] = $this->transarmorToken;
        /*
         * This should always be set to false
         * With it being false, the token can be used authorize, purchase, and reversals (capture, void, and refund)
         * Setting it to true will only allow the token to be used for authorize
         */
        $params['auth'] = 'false';
        $params['type'] = 'FDToken';

        $result = $this->runTransaction($params, '/transactions/tokens', $isApiTest);

        $paymentId = null;
        if (isset($result['token']['value'])) {
            $paymentId = $result['token']['value'];
        } else {
            //Throw an error if there no token value returned
            throw new PaymentException(__('No token value returned'));
        }

        return $paymentId;
    }

    /**
     * These should be implemented by the child gateway.
     *
     * @param \ParadoxLabs\TokenBase\Api\Data\CardInterface $card
     * @return $this
     * @throws PaymentException
     */
    public function setCard(\ParadoxLabs\TokenBase\Api\Data\CardInterface $card)
    {
        $this->setParameter('token_data_value', $card->getPaymentId());
        $cardType = $this->helper->mapCcTypeToFirstData($card->getAdditional('cc_type'));
        $this->setParameter('credit_card_type', $cardType);

        $cardHolderName = $card->getAddress('firstname') . ' ' . $card->getAddress('lastname');
        $this->setParameter('cardholder_name', $cardHolderName);

        $this->setParameter(
            'exp_date',
            sprintf('%02d%02d', $card->getAdditional('cc_exp_month'), substr($card->getAdditional('cc_exp_year'), -2))
        );

        $this->setCardBillingInfo($card);
        parent::setCard($card);

        return $this;
    }

    /**
     * Set the billing information from the card
     *
     * @param \ParadoxLabs\TokenBase\Api\Data\CardInterface $card
     * @return $this
     * @throws PaymentException
     */
    public function setCardBillingInfo(\ParadoxLabs\TokenBase\Api\Data\CardInterface $card)
    {
        $address = $card->getAddressObject();

        $region  = $address->getRegion()->getRegionCode() ?: $address->getRegion()->getRegion();

        $this->setParameter('billing_address_street', implode(', ', $address->getStreet()));
        $this->setParameter('billing_address_city', $address->getCity());
        $this->setParameter('billing_address_state_province', $region);
        $this->setParameter('billing_address_zip_postal_code', $address->getPostcode());
        $this->setParameter('billing_address_country', $address->getCountryId());
        $this->setParameter('billing_address_phone_number', $address->getTelephone());
        $this->setParameter('billing_address_email', $card->getCustomerEmail());

        return $this;
    }

    /**
     * Run an auth transaction for $amount with the given payment info
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return \ParadoxLabs\TokenBase\Model\Gateway\Response
     * @throws LocalizedException
     * @throws PaymentException
     */
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->setParameter('transaction_type', 'authorize');
        $this->setParameter('amount', $amount);
        $this->setParameter('merchant_ref', $payment->getOrder()->getIncrementId());
        $this->setParameter('currency', $payment->getOrder()->getBaseCurrencyCode());

        if ($payment->hasData('cc_cid') && $payment->getData('cc_cid') != '') {
            $this->setParameter('cvv', $payment->getData('cc_cid'));
        }

        $result = $this->createTransaction();
        $response = $this->interpretTransaction($result);

        return $response;
    }

    /**
     * Run a capture transaction for $amount with the given payment info
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @param string $transactionId
     * @return \ParadoxLabs\TokenBase\Model\Gateway\Response
     * @throws LocalizedException
     * @throws PaymentException
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount, $transactionId = null)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */

        if ($this->getHaveAuthorized()) {
            $this->setParameter('transaction_type', 'capture');

            if ($transactionId !== null) {
                $this->setParameter('transId', $transactionId);
            } else {
                $this->setParameter('transId', $payment->getData('transaction_id'));
            }

            if ($payment->getAdditionalInformation('reference_transaction_id') != '') {
                $this->setParameter('transaction_tag', $payment->getAdditionalInformation('reference_transaction_id'));
            }
        }

        if ($this->getHaveAuthorized() === false || empty($this->getTransactionId())) {
            $this->setParameter('transaction_type', 'purchase');
            $this->setParameter('transaction_tag', null);
        }

        $this->setParameter('amount', $amount);
        $this->setParameter('merchant_ref', $payment->getOrder()->getIncrementId());
        $this->setParameter('currency', $payment->getOrder()->getBaseCurrencyCode());

        if ($payment->hasData('cc_cid') && $payment->getData('cc_cid') != '') {
            $this->setParameter('cvv', $payment->getData('cc_cid'));
        }

        $result = $this->createTransaction();
        $response = $this->interpretTransaction($result);

        /**
         * Check for and handle 'transaction not found' error (expired authorization).
         */
        if (in_array('307', $this->getResponseCodes(), false) && $this->getParameter('transId') != '') {
            $this->helper->log(
                $this->code,
                sprintf("Transaction not found. Attempting to recapture.\n%s", json_encode($response->getData()))
            );

            $this->setParameter('transId', null)
                 ->setHaveAuthorized(false)
                 ->setCard($this->getData('card'));

            $response = $this->capture($payment, $amount, '');
        }

        return $response;
    }

    /**
     * Run a refund transaction for $amount with the given payment info
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @param string $transactionId
     * @return \ParadoxLabs\TokenBase\Model\Gateway\Response
     * @throws LocalizedException
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount, $transactionId = null)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */

        $this->setParameter('transaction_type', 'refund');
        $this->setParameter('amount', $amount);
        $this->setParameter('merchant_ref', $payment->getOrder()->getIncrementId());
        $this->setParameter('currency', $payment->getOrder()->getBaseCurrencyCode());

        if ($transactionId !== null) {
            $this->setParameter('transId', $transactionId);
        } elseif ($payment->getTransactionId() != '') {
            $this->setParameter('transId', $payment->getTransactionId());
        }

        if ($payment->getAdditionalInformation('reference_transaction_id') != '') {
            $this->setParameter('transaction_tag', $payment->getAdditionalInformation('reference_transaction_id'));
        }

        $result = $this->createTransaction();
        $response = $this->interpretTransaction($result);

        return $response;
    }

    /**
     * Run a void transaction for the given payment info
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param string $transactionId
     * @return \ParadoxLabs\TokenBase\Model\Gateway\Response
     * @throws LocalizedException
     * @throws PaymentException
     */
    public function void(\Magento\Payment\Model\InfoInterface $payment, $transactionId = null)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */

        $this->setParameter('transaction_type', 'void');

        if ($transactionId !== null) {
            $this->setParameter('transId', $transactionId);
        } elseif ($payment->getTransactionId() != '') {
            $this->setParameter('transId', $payment->getTransactionId());
        }

        /**
         * If the payment amount doesn't match the authorization amount, we have a partial invoice and a
         * new authorization transaction_tag and amount to deal with
         */
        if ($payment->getOrigData('base_amount_authorized') !== $payment->getAdditionalInformation('amount')) {
            $newAuthInfo = $payment->getAuthorizationTransaction()->getAdditionalInformation('raw_details_info');

            $this->setParameter('amount', $newAuthInfo['amount']);

            if ($newAuthInfo['reference_transaction_id'] != '') {
                $this->setParameter('transaction_tag', $newAuthInfo['reference_transaction_id']);
            }
        } else {
            if ($payment->getAdditionalInformation('reference_transaction_id') != '') {
                $this->setParameter('transaction_tag', $payment->getAdditionalInformation('reference_transaction_id'));
            }
            $this->setParameter('amount', $payment->getAdditionalInformation('amount'));
        }

        $this->setParameter('merchant_ref', $payment->getOrder()->getIncrementId());
        $this->setParameter('currency', $payment->getOrder()->getBaseCurrencyCode());

        $result = $this->createTransaction();
        $response = $this->interpretTransaction($result);

        return $response;
    }

    /**
     * This does not do anything for First Data, but it is needed since it is an abstract method in the parent class
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param string $transactionId
     * @return \ParadoxLabs\TokenBase\Model\Gateway\Response
     */
    public function fraudUpdate(\Magento\Payment\Model\InfoInterface $payment, $transactionId)
    {
        $response = $this->responseFactory->create();

        return $response;
    }

    /**
     * Get transaction ID.
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->getParameter('transId');
    }

    /**
     * Set prior transaction ID for next transaction.
     *
     * @param $transactionId
     * @return $this
     * @throws PaymentException
     */
    public function setTransactionId($transactionId)
    {
        $this->setParameter('transId', $transactionId);

        return $this;
    }

########################################################################################################################
#### API methods: See the First Data documentation.
#### https://developer.payeezy.com/apis
########################################################################################################################

    /**
     * Run an actual transaction with First Data with stored data.
     *
     * @return array
     * @throws LocalizedException
     */
    public function createTransaction()
    {
        $type = $this->getParameter('transaction_type');

        /**
         * Initialize our params array.
         *
         * NOTE: All elements in the XML array are order-sensitive!
         */
        $params = [];

        /**
         * Define the transaction and basics: Amount, txn ID, auth code
         */
        $params = $this->createTransactionAddTransactionInfo($params, $type);

        /**
         * Add payment info.
         */
        $params = $this->createTransactionAddPaymentInfo($params);

        /**
         * Add customer info.
         */
        $params = $this->createTransactionAddCustomerInfo($params);

        return $this->runTransaction($params, '/transactions');
    }

    /**
     * Turn the transaction response into an array, as best we can.
     *
     * @param array $response
     * @return array
     * @throws PaymentException
     */
    protected function getDataFromResponse($response)
    {
        if (empty($response)) {
            $this->helper->log(
                $this->code,
                sprintf("First Data Gateway: Transaction failed; no response.\n%s", $this->log)
            );

            throw new PaymentException(
                __('First Data Gateway: Transaction failed; no response. '
                    . 'Please re-enter your payment info and try again.')
            );
        }

        /**
         * Turn the array into a keyed object and infer some things.
         */
        $data = [
            'response_code'            => (int)$this->helper->getArrayValue($response, 'gateway_resp_code'),
            'response_subcode'         => '',
            'response_reason_code'     => (int)$this->helper->getArrayValue($response, 'Error/messages/0/code'),
            'response_reason_text'     => $this->helper->getArrayValue($response, 'Error/messages/0/description'),
            'transaction_id'           => $this->helper->getArrayValue($response, 'transaction_id'),
            'reference_transaction_id' => $this->helper->getArrayValue($response, 'transaction_tag'),
            'amount'                   => $this->getParameter('amount'),
            'method'                   => 'CC',
            'transaction_type'         => $this->getParameter('transaction_type'),
            'card_code_response_code'  => $this->helper->getArrayValue($response, 'cvv2'),
            'avs_result_code'          => $this->helper->getArrayValue($response, 'avs'),
            'card_type'                => $this->getParameter('credit_card_type'),
            'payment_id'               => $this->getParameter('token_data_value'),
            'is_fraud'                 => false,
            'is_error'                 => false,
            'correlation_id'           => $this->helper->getArrayValue($response, 'correlation_id'),
            'bank_resp_code'           => $this->helper->getArrayValue($response, 'bank_resp_code'),
            'bank_message'             => $this->helper->getArrayValue($response, 'bank_message'),
            'gateway_resp_code'        => $this->helper->getArrayValue($response, 'gateway_resp_code'),
            'gateway_message'          => $this->helper->getArrayValue($response, 'gateway_message'),
        ];

        return $data;
    }

    /**
     * Add payment info to a createTransaction API request's parameters.
     *
     * Split out to reduce that method's cyclomatic complexity.
     *
     * @param array $params
     * @param string $type
     * @return array
     */
    protected function createTransactionAddTransactionInfo($params, $type)
    {
        $params['transaction_type'] = $type;

        if ($this->hasParameter('amount')) {
            // Amount is expected to be in cents
            $params['amount'] = static::formatAmount($this->getParameter('amount'));
        }

        if ($this->hasParameter('transaction_tag')) {
            $params['transaction_tag'] = $this->getParameter('transaction_tag');
        }

        if ($this->hasParameter('merchant_ref')) {
            $params['merchant_ref'] = $this->getParameter('merchant_ref');
        }

        $params['currency_code'] = $this->getParameter('currency');

        $params['method'] = 'token';

        return $params;
    }

    /**
     * Add payment info to a createTransaction API request's parameters.
     *
     * Split out to reduce that method's cyclomatic complexity.
     *
     * @param array $params
     * @return array
     */
    protected function createTransactionAddPaymentInfo($params)
    {
        $params['token'] = [
            'token_type' => 'FDToken',
            'token_data' => [
                'type'     => $this->getParameter('credit_card_type'),
                'value' => $this->getParameter('token_data_value'),
                'cardholder_name' => $this->getParameter('cardholder_name'),
                'exp_date' => $this->getParameter('exp_date'),
            ],
        ];

        /**
         * Send the CVV if it is set
         */
        if ($this->hasParameter('cvv')) {
            $params['token']['token_data']['cvv'] = $this->getParameter('cvv');
        }

        return $params;
    }

    /**
     * Add payment info to a tokenizeCreditCard API request's parameters.
     *
     * Split out to reduce that method's cyclomatic complexity.
     *
     * @param array $params
     * @return array
     */
    protected function tokenizeCreditCardAddPaymentInfo($params)
    {
        $params['credit_card'] = [
            'type' => $this->getParameter('credit_card_type'),
            'cardholder_name' => $this->getParameter('cardholder_name'),
            'card_number' => $this->getParameter('card_number'),
            'exp_date' => $this->getParameter('exp_date'),
            'cvv' => $this->getParameter('cvv')
        ];

        return $params;
    }

    /**
     * Add item info to a createTransaction API request's parameters.
     *
     * Split out to reduce that method's cyclomatic complexity.
     *
     * @param array $params
     * @return array
     */
    protected function createTransactionAddCustomerInfo($params)
    {
        $params['billing_address'] = [
            'street' => $this->getParameter('billing_address_street'),
            'city' => $this->getParameter('billing_address_city'),
            'state_province' => $this->getParameter('billing_address_state_province'),
            'zip_postal_code' => $this->getParameter('billing_address_zip_postal_code'),
            'country' => $this->getParameter('billing_address_country'),
            'email' => $this->getParameter('billing_address_email'),
            'phone' => [
                'number'     => $this->getParameter('billing_address_phone_number'),
            ],
        ];

        return $params;
    }

    /**
     * After running a transaction, handle the response.
     *
     * Split out to reduce that method's cyclomatic complexity.
     *
     * @param \Magento\Framework\HTTP\ZendClient $httpClient
     * @param array $params
     * @param bool $isApiTest
     * @throws LocalizedException
     */
    protected function handleResponse($httpClient, $params, $isApiTest = false)
    {
        if (!empty($this->lastResponse)) {
            $this->lastResponse = json_decode($this->lastResponse, true);

            $this->log .= 'REQUEST: ' . $this->sanitizeLog($params) . "\n";
            $this->log .= 'RESPONSE: ' . $this->sanitizeLog($this->lastResponse) . "\n";

            if ($this->testMode === true && !$isApiTest) {
                $this->helper->log($this->code, $this->log, true);
            }

            /**
             * Check for basic errors.
             */
            $this->handleErrors($isApiTest);
        } else {
            $this->helper->log(
                $this->code,
                sprintf(
                    "CURL Connection error: %s (%s)\nREQUEST: %s",
                    $httpClient->getAdapter()->getError(),
                    $httpClient->getAdapter()->getErrno(),
                    $this->sanitizeLog($params)
                )
            );

            throw new LocalizedException(
                __(sprintf(
                    'First Data Gateway Connection error: %s (%s)',
                    $httpClient->getAdapter()->getError(),
                    $httpClient->getAdapter()->getErrno()
                ))
            );
        }
    }

    /**
     * After running a transaction, handle any generic errors in the response.
     *
     * Split out to reduce that method's cyclomatic complexity.
     *
     * @param $isApiTest
     * @return void
     * @throws PaymentException
     */
    protected function handleErrors($isApiTest)
    {
        /**
         * Get error message(s), if any -- they can be returned a number of ways.
         */
        $responses = $this->getResponseMessages();

        if (isset($responses) && !empty($responses)) {
            $messages = [];
            foreach ($responses as $response) {
                if (in_array($response['code'], ['00', '100'], false)) {
                    continue;
                }

                /**
                 * Trying to void but authorization is not found -- ignore it and continue with the voiding process
                 */
                if (in_array($response['code'], ['307', '400'], false)
                    && $this->getParameter('transaction_type') === 'void') {
                    $this->helper->log(
                        $this->code,
                        sprintf("Could not find transaction to void; skipping.\n%s", $this->log)
                    );

                    return;
                }

                $messages[] = sprintf('%s (%s)', $response['description'], $response['code']);
            }

            if (!empty($messages)) {
                if (!$isApiTest) {
                    $this->helper->log(
                        $this->code,
                        sprintf("API error: %s\n%s", implode(',', $messages), $this->log)
                    );
                }

                throw new PaymentException(__(sprintf('First Data Gateway: %s', implode(',', $messages))));
            }
        }
    }

    /**
     * Create the header for the transactions
     *
     * @param array $auth
     * @param array $paymentData
     * @return array
     */
    protected function createHeader($auth, $paymentData)
    {
        $apiKey = $auth['apikey'];
        $token = $auth['token'];
        $apiSecret = $this->secretKey;

        $nonce = (string)hexdec(bin2hex(openssl_random_pseudo_bytes(4)));
        $timestamp = number_format(round(microtime(true) * 1000), 0, '.', '');

        $data = $apiKey . $nonce . $timestamp . $token . $paymentData;

        $hashAlgorithm = 'sha256';
        // Make sure the HMAC hash is in hex
        $hmac = hash_hmac($hashAlgorithm, $data, $apiSecret, false);

        // Authorization : base64 of hmac hash
        $hmac_enc = base64_encode($hmac);

        $header = [
            'Content-Type: application/json',
            'apikey: '. (string)$apiKey,
            'token: '. (string)$token,
            'Authorization: '. $hmac_enc,
            'nonce: '. $nonce,
            'timestamp: '. $timestamp,
        ];

        return $header;
    }

    /**
     * Format amount to the appropriate precision.
     *
     * @param float $amount
     * @return int
     */
    public static function formatAmount($amount)
    {
        // Amount is expected to be in cents
        return round($amount * 100);
    }

    /**
     * Get response and error messages and codes
     *
     * @return array|mixed
     */
    protected function getResponseMessages()
    {
        $errors = [];

        if (isset($this->lastResponse['Error']['messages'])) {
            $errors = $this->helper->getArrayValue($this->lastResponse, 'Error/messages');
        } elseif (isset($this->lastResponse['results']['Error']['messages'])) {
            $errors = $this->helper->getArrayValue($this->lastResponse, 'results/Error/messages');
        }

        if (isset($this->lastResponse['gateway_resp_code'])) {
            $errors[] = [
                'code' => $this->lastResponse['gateway_resp_code'],
                'description' => $this->lastResponse['gateway_message'],
            ];
        }

        if (isset($this->lastResponse['bank_resp_code'])) {
            $errors[] = [
                'code' => $this->lastResponse['bank_resp_code'],
                'description' => $this->lastResponse['bank_message'],
            ];
        }

        return $errors;
    }

    /**
     * Get response codes
     *
     * @return array
     */
    protected function getResponseCodes()
    {
        $codes = [];
        $messages = $this->getResponseMessages();
        foreach ($messages as $message) {
            $codes[] = $message['code'];
        }

        return $codes;
    }
}
