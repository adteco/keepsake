<?php

    namespace Keepsake\UnitsOfMeasure\Plugin\Pricing\Render;

    use Magento\Framework\Pricing\Amount\AmountInterface;

    class PriceBox
    {
        /**
         * @var \Magento\Catalog\Api\ProductRepositoryInterface
         */
        protected $productRepository;

        /**
         * PriceBox constructor.
         *
         * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
         */
        public function __construct(\Magento\Catalog\Api\ProductRepositoryInterface $productRepository)
        {
            $this->productRepository = $productRepository;
        }

        public function aroundRenderAmount(
            \Magento\Framework\Pricing\Render\PriceBox $subject,
            \Closure $proceed,
            AmountInterface $amount,
            array $arguments = []
        ) {
            $html = $proceed($amount, $arguments);
            $product = $this->productRepository->getById($subject->getSaleableItem()->getId());
            $unit = $product->getUnit() ? $product->getAttributeText('unit') : '';
            if ($unit) {
                $html .= '<div>' . __('per') . ' ' .$unit . '</div>';;
            }

            return $html;
        }
    }