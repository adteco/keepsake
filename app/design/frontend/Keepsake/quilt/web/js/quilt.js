 require([
    'jquery'
], function ($) {
    (function() {
        var ev = new $.Event('classadded'),
            orig = $.fn.addClass;
        $.fn.addClass = function() {
            $(this).trigger(ev, arguments);
            return orig.apply(this, arguments);
        }
    })();
    $.fn.extend({
        scrollToMe: function(){
            if($(this).length){
                var top = $(this).offset().top - 100;
                $('html,body').animate({scrollTop: top}, 300);
            }
        },
        scrollToJustMe: function(){
            if($(this).length){
                var top = jQuery(this).offset().top;
                $('html,body').animate({scrollTop: top}, 300);
            }
        }
    });
    $(document).ready(function(){
    // This button will decrement the value till 0
        $(document).on("click", ".qtyminus-half", function(e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');
            // Get its current value
            var currentVal = parseInt($(this).parents('form').find('input[name="qty"]').val());
            // If it isn't undefined or its greater than 0
            if (!isNaN(currentVal) && currentVal > 0) {
                // Decrement one
                $(this).parents('form').find('input[name="qty"]').val(currentVal - 0.5);
            } else {
                // Otherwise put a 0 there
                $(this).parents('form').find('input[name="qty"]').val(0);
            }
        });
        $(".qty-inc-half").unbind('click').click(function(){
            if($(this).parents('.field.qty').find("input.input-text.qty").is(':enabled')){
                $(this).parents('.field.qty').find("input.input-text.qty").val((+$(this).parents('.field.qty').find("input.input-text.qty").val() + 0.5) || 0);
                $(this).parents('.field.qty').find("input.input-text.qty").trigger('change');
                $(this).focus();
            }
        });
        $(".qty-dec-half").unbind('click').click(function(){
            if($(this).parents('.field.qty').find("input.input-text.qty").is(':enabled')){
                $(this).parents('.field.qty').find("input.input-text.qty").val(($(this).parents('.field.qty').find("input.input-text.qty").val() - 0.5 > 0) ? ($(this).parents('.field.qty').find("input.input-text.qty").val() - 0.5) : 0);
                $(this).parents('.field.qty').find("input.input-text.qty").trigger('change');
                $(this).focus();
            }
        });
    });
});
 
